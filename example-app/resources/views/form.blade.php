<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SanberBook</title>
</head>
<body>
    <form action="/welcome" method="POST">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>

        <label>First Name:</label> <br>
        <input type="text" name="first"> <br><br>

        <label>Last Name:</label> <br>
        <input type="text" name="last"> <br><br>

        <label>Gender:</label> <br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Other <br><br>

        <label>Nationality:</label> <br>
        <select name="nation">
            <option value="indo">Indonesian</option>
            <option value="malay">Malaysian</option>
            <option value="ausie">Australian</option>
        </select> <br><br>

        <label>Language Spoken:</label> <br>
        <input type="checkbox" name="lang">Bahasa Indonesia <br> 
        <input type="checkbox" name="lang">English <br>
        <input type="checkbox" name="lang">Other <br>

        <label>Bio:</label> <br>
        <textarea name="bio" rows="10" cols="30"></textarea> <br>

        <input type="submit">

    </form>
    
</body>
</html>