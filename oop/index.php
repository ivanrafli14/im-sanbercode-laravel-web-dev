<?php
require_once ('animal.php');
require_once ('frog.php');
require_once ('ape.php');

$sheep = new Animal("shaun");

 $sheep->get_name(); // "shaun"
 $sheep->get_legs(); // 4
 $sheep->get_cold_blooded(); 

echo "<br>";

$kodok = new Frog("buduk");
$kodok->get_name(); // "shaun"
$kodok->get_legs(); // 4
$kodok->get_cold_blooded();
$kodok->jump() ; // "hop hop"

echo "<br>";

$sungokong = new Ape("kera sakti");
$sungokong->get_name(); // "shaun"
$sungokong->get_legs(); // 4
$sungokong->get_cold_blooded();
$sungokong->yell(); // "Auooo"