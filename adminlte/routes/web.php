<?php

use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;
use PhpParser\Node\Expr\Cast;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/cast', [CastController::class, 'index']);

Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);
Route::put('/cast/{cast_id}', [CastController::class, 'update']);


Route::get('/', function () {
    return view('home', [
        'title' => 'Home'
    ]);
});

Route::get('/table', function(){
    return view('table', [
        'title' => 'Table'
    ]);
});

Route::get('/data-table', function(){
    return view('data-table', [
        'title' => 'Data Table'
    ]);
});

Route::get('/cast/create', [CastController::class, 'create']);
Route::post('/cast', [CastController::class, 'store']);
Route::get('/cast/{cast_id}', [CastController::class, 'show']);
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);



