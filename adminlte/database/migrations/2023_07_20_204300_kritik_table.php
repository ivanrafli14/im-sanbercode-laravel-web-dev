<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kritik', function(Blueprint $table){
            $table->id();
            // $table->unsignedBigInteger('user_id');
            // $table->unsignedBigInteger('film_id');
            $table->text('content');
            $table->text('point');

            // $table->foreign('user_id')->references('id')->on('users');
            // $table->foreign('film_id')->references('id')->on('film');

            $table->foreignId('user_id')->constrained();
            $table->foreignId('film_id')->constrained(
                table: 'film',
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kritik');
    }
};
