<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index(){
        $cast = DB::table('cast') -> get();

        return view('cast',[
            'title' => 'cast',
            'cast' => $cast
        ]);
    }

    public function create(){
        return view('createcast', ['title'=>' Tambah Cast']);
    }

    public function store(Request $request){
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast');
    }

    public function show($cast_id){
        $filter = DB::table('cast')->where('id', $cast_id)->get();
        //dd($filter);
        return view('cast', ['cast' => $filter,'title' => 'Detail Cast',]);
    }

    public function edit($cast_id){
        $filter = DB::table('cast')->where('id', $cast_id)->first();
       
        //dd($filter);
        return view('editcast', ['cast' => $filter,'title' => 'Update cast',]);
    }

    public function update(Request $request, $cast_id){
        //return ("Hallo");
        
        
         DB::table('cast')->where('id',$cast_id)->update([
            'nama' => $request->nama,
            'umur' => $request->umur,
            'bio' => $request->bio
        ]);

        return redirect('/cast');
    }


    public function destroy($cast_id){

      
	
	    DB::table('cast')->where('id',$cast_id)->delete();
		
	    return redirect('/cast');
    }
}
