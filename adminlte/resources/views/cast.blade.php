@extends('layouts.master')

@section('main-content')
    <a class="btn btn-primary ml-2 mb-4" href="/cast/create" role="button"><i class="fas fa-plus"></i>Tambah Cast</a>
    <div class="row bg-primary ml-3 mr-3 font-weight-bold">
        <div class="col-1">No.</div>
        <div class="col-3">Nama</div>
        <div class="col-2">Umur</div>
        <div class="col-3">Biodata</div>
        <div class="col-3">Aksi</div>
    </div>

    <div class="row bg-light ml-3 mr-3 font-weight-bold">
        @foreach ($cast as $el)
        <div class="col-1 mt-2">{{$el->id}}</div>
        <div class="col-3 mt-2">{{$el->nama}}</div>
        <div class="col-2 mt-2">{{$el->umur}}</div>
        <div class="col-3 mt-2">{{$el->bio}}</div>
        <div class="col-3 mt-2 mb-2">
            {{-- <a class="btn btn-primary" href="" role="button"><i class = "fa fa-trash"></i>Hapus</a> --}}
            <form action='/cast/{{$el->id}}' method="POST">
                @method('DELETE')
                @csrf
             <a class="btn btn-warning mr-2" href="/cast/{{$el->id}}/edit" role="button"><i class="fas fa-edit"></i>Edit</a>

                <button class="btn btn-danger" type="submit"><i class = "fa fa-trash"></i>Hapus</button>
            </form>
        </div>
       
        @endforeach
       
    </div>
    

   

    
@endsection