 @extends('layouts.master')

@section('main-content')

<h3>Update Data</h3>

<form class="ml-2 mr-2" action="/cast/{{$cast->id}}" method="POST">
    @method('PUT')
    @csrf

    <div class="form-group ">
      <label for="exampleInputEmail1">Nama</label>
      <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" >
     
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Umur</label>
      <input type="number" class="form-control" name="umur" value="{{$cast->umur}}">
    </div>
    
    <div class="form-group">
      <label for="exampleInputtext1">Bio</label>
      <input type="text" class="form-control" name="bio" value="{{$cast->bio}}">
    </div>
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

  @endsection