@extends('layouts.master')

@section('main-content')

<form class="ml-2 mr-2" action="/cast" method="post">
  @csrf
    <div class="form-group ">
      <label for="exampleInputEmail1">Nama</label>
      <input type="text" class="form-control" name="nama" >
     
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Umur</label>
      <input type="number" class="form-control" name="umur">
    </div>
    
    <div class="form-group">
      <label for="exampleInputtext1">Bio</label>
      <input type="text" class="form-control" name="bio">
    </div>
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

  @endsection